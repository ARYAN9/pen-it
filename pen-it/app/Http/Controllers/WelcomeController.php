<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    //simplePaginate remove 1,2,3... in paginate and brings next and previous!!!

    public function index(){
        // dd(Post::search()->published()->latest('published_at')->simplePaginate(2));
        return view('blog.index',[
            'categories'=>Category::all(),
            'posts'=>Post::search()->published()->latest('published_at')->simplePaginate(2),
            'tags'=>Tag::all()
        ]);
    }
    public function show(Post $post){
        // dd($post->search());
        $categories = Category::all();
        $tags = Tag::all();
        return view('blog.post', compact([
            'post',
            'tags',
            'categories'
        ]));
    }
    public function category(Category $category){
        return view('blog.index',[
            'categories'=>Category::all(),
            'posts'=>$category->posts()->search()->simplePaginate(2),
            'tags'=>Tag::all()
        ]);
    }
    public function tag(Tag $tag){
        return view('blog.index',[
            'categories'=>Category::all(),
            'posts'=>$tag->posts()->search()->simplePaginate(2),
            'tags'=>Tag::all()
        ]);
    }
}
