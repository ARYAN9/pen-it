<?php

namespace App\Http\Middleware;

use App\Post;
use Closure;

class VerifyAuthorForEditAndDeletePost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(is_object($request->post)){
            if(!($request->post->user_id == auth()->id())){
                return redirect(abort(401));
            }
        }
        elseif(is_numeric($request->post))
        {
            //Because the $request->post is the post_id [i.e id of the post table]
            //So, first we bring the post object of that id and then pick the user_id from it!!!
            if(!((Post::onlyTrashed()->findOrFail($request->post))->user_id == auth()->id())){
                return redirect(abort(401));
            }
        }
        return $next($request);
    }
}
