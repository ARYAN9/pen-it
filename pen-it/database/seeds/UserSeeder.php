<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email','aryan@gmail.com')->get()->first();
        if(!$user){
            \App\User::create([
                'name'=>'Aryan Dhami',
                'email'=>'aryan@gmail.com',
                'password'=>\Illuminate\Support\Facades\Hash::make('aryan12345'),
                'role'=>'admin'
            ]);
        }
        else
        {
            $user->update(['role'=>'admin']);
        }
        
        \App\User::create([
           'name'=>'John Doe',
            'email'=>'john.doe@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('john123'),
        ]);
        
        \App\User::create([
           'name'=>'Jane Doe',
            'email'=>'jane.doe@gmail.com',
            'password'=>\Illuminate\Support\Facades\Hash::make('jane123'),
        ]);
    }
}