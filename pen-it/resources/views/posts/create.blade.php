@extends('layouts.app')

@section('content')

    <div class="card">
        <div class="card-header">
            <h5 class="float-left text-primary">Add Post</h5>
            
            <div class="float-right btn btn-primary btn-sm"><div onclick="displayModalForm()" data-toggle="modal" data-target="#deleteModal">Live Preview </div></div>  
        </div>

        <div class="card-body">
            <form action="{{ route('posts.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text"
                           value="{{ old('title') }}"
                           class="form-control @error('title') is-invalid @enderror"
                           name="title" id="title">
                    @error('title')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="category_id">Select Category</label>
                    <select name="category_id" id="category_id" class="form-control">
                        <option value="0" selected>Select Category</option>
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    @error('category_id')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="tags[]">Select Tags</label>
                    <select name="tags[]" id="tags" class="form-control" multiple>
                        @foreach($tags as $tag)
                            <option value="{{$tag->id}}">{{$tag->name}}</option>
                        @endforeach
                    </select>
                    @error('tags')
                    <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="excerpt">Excerpt</label>
                    <textarea name="excerpt"
                              id="excerpt"
                              class="form-control @error('excerpt') is-invalid @enderror"
                              rows="4">{{ old('excerpt') }}</textarea>
                    @error('excerpt')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <input type="hidden" name="content" id="content">
                    <trix-editor input="content"></trix-editor>
                    @error('content')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="published_at">Published At</label>
                    <input type="date"
                           value="{{ old('published_at') }}"
                           class="form-control"
                           name="published_at" id="published_at">
                    @error('published_at')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="image">Image</label>
                    <input type="file"
                           value="{{ old('image') }}"
                           class="form-control @error('image') is-invalid @enderror"
                           name="image" id="image">
                    @error('image')
                        <p class="text-danger">{{ $message }}</p>
                    @enderror
                </div>
                <div class="form-group">
                    <button class="btn btn-success" type="submit">Add Post</button>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Delete Modal</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" METHOD="POST" id="deleteForm">
                    @csrf
                    @method('DELETE')
                    <div class="modal-body">
                        <p>Are you sure you want to delete Post??</p>
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-12 mb50 align-content-lg-stretch">
                                <h4 id="blog-title"></h4>
                                <div class="blog-three-attrib">
                                    <span class=" icon-pencil"></span> <a href="#" id="blog-author-name">{{auth()->user()->name}}</a>
                                </div>
                                <img src="" class="img-responsive" width="200%" alt="image blog" id="blog-image">
                                <p class="mt25" id="blog-excerpt" >
                                    
                                </p>
                                <div class="blog-post-read-tag mt50">
                                    <i class="fa fa-tags"></i> Tags:
                                    <a href="" id="blog-tag-list"></a> 
                                </div>
                                <a href="" class="button button-gray button-xs">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger">Delete Post</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script>
        function displayModalForm(){
            // $("#deleteForm").attr('action', "wow.html");
            $("#blog-title").html($("#title").val());
            $("#blog-excerpt").html($("excerpt").val());
            $("#blog-author-name").html();
            console.log($("#image").prop('files'));
            
            console.log($("#tags").val());
            var tag_list = $("#tags").val();
            var all_tags = "";
            for(var i=0;i<tag_list.length;i++){
                console.log(tag_list[i]);
                var index = tag_list[i] - 1;
                console.log($("#tags")[0][index].innerHTML);
                all_tags = all_tags + $("#tags")[0][index].innerHTML + ",";
            }
            console.log(all_tags);
            
            $("#blog-tag-list").html(all_tags.slice(0, -1));
            // console.log($("#tags")[0][0].innerHTML);
            
        }
        $('#image').change( function(event) {
            $("#blog-image").fadeIn("fast").attr('src',URL.createObjectURL(event.target.files[0]));
        });
        flatpickr("#published_at", {
            enableTime: true
        });
        $(document).ready(function() {
            $('#category_id').select2();
        });
        $(document).ready(function() {
            $('#tags').select2();
        });
    </script>
@endsection
@section('page-level-styles')
<link rel="stylesheet" href="{{asset('assets/css/main/main.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/main/setting.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/main/hover.css')}}">


   <link rel="stylesheet" href="{{asset('assets/css/color/pasific.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/icon/font-awesome.css')}}">
   <link rel="stylesheet" href="{{asset('assets/css/icon/et-line-font.css')}}">
   
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.3/trix.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endsection





    